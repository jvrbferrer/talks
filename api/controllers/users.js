const User = require("../models/User");
const axios = require("axios");
const { inspectFBToken, getFBProfileInfo } = require("../utils/APICalls");

exports.authenticate = async function (req, res, next) {
  const facebookToken = req.body.facebookToken;
  try {
    const inspectRes = await inspectFBToken(facebookToken);
    if (!!inspectRes.data.data.error || !inspectRes.data.data.is_valid) {
      console.log("ERROR", inspectRes.data.data.error, facebookToken);
      throw Error("Can't access info from facebook");
    }
    const profileInfo = await getFBProfileInfo(facebookToken);
    if (
      !profileInfo.data.email ||
      !profileInfo.data.name ||
      !profileInfo.data.picture.data.url
    ) {
      throw Error("Can't access info from facebook account");
    }
    let name = profileInfo.data.name;
    let email = profileInfo.data.email;
    let profileImage = profileInfo.data.picture.data.url;
    let foundUser = await User.findOne({ email });
    if (!foundUser) {
      foundUser = await User.create({
        email,
        name,
        profileImage,
      });
    }
    const userToken = await foundUser.generateToken();
    return res.json({
      newToken: userToken,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      error: {
        _main:
          "A problem occurred while processing the request. Try again later.",
      },
    });
  }
};

exports.getImages = async function (req, res, next) {
  const userId = req.params.userId;
  try {
    const user = await User.findById(userId);
    const userImages = user.getImages();
    return res.json({
      userImages,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      error: {
        _main:
          "A problem occurred while processing the request. Try again later.",
      },
    });
  }
};

exports.createImage = async function (req, res, next) {
  const userId = req.params.userId;
  const imageName = req.body.imageName;
  const token = req._token;
  try {
    const user = await User.findById(userId);
    const postData = await user.createImage(imageName);
    return res.json({
      postData,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      error: {
        _main:
          "A problem occurred while processing the request. Try again later.",
      },
    });
  }
};

exports.deleteImage = async function (req, res, next) {
  const userId = req.params.userId;
  const imageKey = req.body.imageKey;

  res.status(500).json({
    error: {
      _main: "Not implemented",
    },
  });
};

exports.getHomepageInfo = async function (req, res, next) {
  const userId = req.params.userId;
  try {
    const user = await User.findById(userId);
    const homepageInfo = await user.getHomepageInfo(req._tokenPayload);
    return res.json(homepageInfo);
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      error: {
        _main:
          "A problem occurred while processing the request. Try again later",
      },
    });
  }
};

exports.getUserpageInfo = async function (req, res, next) {
  const userId = req.params.userId;
  try {
    const user = await User.findById(userId);
    const userpageInfo = await user.getUserpageInfo(req._tokenPayload);
    return res.json(userpageInfo);
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      error: {
        _main:
          "A problem occurred while processing the request. Try again later",
      },
    });
  }
};

exports.updateDescription = async function (req, res, next) {
  const userId = req.params.userId;
  const newDescription = req.body.newDescription;
  try {
    const user = await User.findById(userId);
    await user.updateDescription(req._tokenPayload, newDescription);
    return res.json({});
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      error: {
        _main:
          "A problem occurred while processing the request. Try again later",
      },
    });
  }
};

exports.updatePreferences = async function (req, res, next) {
  const userId = req.params.userId;
  const { units, ...newPreferences } = req.body.newPreferences;
  try {
    const user = await User.findById(userId);
    await user.updatePreferences(req._tokenPayload, newPreferences, units);
    return res.json({});
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      error: {
        _main:
          "A problem occurred while processing the request. Try again later",
      },
    });
  }
};
