const Conversation = require("../models/Conversation");

exports.get = async function (req, res, next) {
  const conversationId = req.params.conversationId;
  try {
    const conversation = await Conversation.get(
      req._tokenPayload,
      conversationId
    );
    conversation.read(req._tokenPayload);
    return res.json(conversation);
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ error: { _main: "An error occurred. Try again later" } });
  }
};
