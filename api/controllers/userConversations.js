const Conversation = require("../models/Conversation");
const Interest = require("../models/Interest");
const UserConversation = require("../models/helpers/UserConversations");

exports.create = async function (req, res, next) {
  const userId = req.params.userId;
  const receiverId = req.body.receiverId;
  const interestId = req.body.interestId;
  try {
    const interest = await Interest.findById(interestId);
    const newConversation = await userConversation.create(req._tokenPayload, {
      users: [
        { id: userId, accepted: true, read: true },
        { id: receiverId, accepted: false, read: false },
      ],
      interest: { id: interestId, name: interest.name },
    });
    return res.json({ conversationId: newConversation._id });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ error: { _main: "An error occurred. Try again later" } });
  }
};

exports.getConversationsPage = async function (req, res, next) {
  const userId = req.params.userId;
  try {
    const userConversations = await UserConversation.getPreviews(
      req._tokenPayload,
      userId
    );
    return res.json({ conversations: userConversations });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ error: { _main: "An error occurred. Try again later" } });
  }
};
