const UserInterests = require("../models/helpers/UserInterests");

exports.create = async function (req, res, next) {
  const userId = req.params.userId;
  const interestId = req.params.interestId;
  const token = req._tokenPayload;

  try {
    await UserInterests.create(token, userId, interestId);
    return res.json({});
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      error: {
        _main:
          "A problem occurred while processing the request. Try again later.",
      },
    });
  }
};

exports.delete = async function (req, res, next) {
  const userId = req.params.userId;
  const interestId = req.params.interestId;
  const token = req._tokenPayload;
  try {
    await UserInterests.delete(token, userId, interestId);
    return res.json({});
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      error: {
        _main:
          "A problem occurred while processing the request. Try again later.",
      },
    });
  }
};
