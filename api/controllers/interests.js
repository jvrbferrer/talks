const Interest = require("../models/Interest");
const crypto = require("crypto");
const User = require("../models/User");
const { populate } = require("../models/User");

exports.create = async function (req, res, next) {
  const name = req.body.name;
  const userId = req._tokenPayload.sub;

  try {
    const interest = await Interest.create({ name, creator: userId });
    return res.json({ _id: interest.id });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      error: {
        _main:
          "An error occurred while creating the interest. Check if an interest with the same name was not created or try again later.",
      },
    });
  }
};

exports.search = async function (req, res, next) {
  const interestName = req.params.interestName;
  try {
    const interestsFound = await Interest.find({
      $text: { $search: interestName },
    }).select("name picture");
    return res.json({
      interests: interestsFound,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      error: {
        _main:
          "A problem occurred while processing the request. Try again later.",
      },
    });
  }
};

exports.suggestImage = async function (req, res, next) {
  const imagename = req.body.imageName;
  const interestId = req.params.interestId;

  try {
    const interest = await Interest.findById(interestId);
    const postData = await interest.addSuggestedImage(imagename);
    return res.json({ postData: postData });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      error: {
        _main: "An error occurred in the server. Try again later.",
      },
    });
  }
};

exports.getSuggestedImages = async function (req, res, next) {
  const interestId = req.params.interestId;
  try {
    const interest = await Interest.findById(interestId);
    const suggestedImages = interest.getSuggestedImages();
    return res.json({ suggestedImages });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      error: {
        _main: "An error occurred in the server. Try again later.",
      },
    });
  }
};

exports.setImage = async function (req, res, next) {
  const interestId = req.params.interestId;
  const newImage = req.body.newImage;
  try {
    const interest = await Interest.findById(interestId);
    await interest.setImage(newImage);
    return res.json({});
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      error: {
        _main: "An error occurred in the server. Try again later.",
      },
    });
  }
};

exports.getInterestedUsers = async function (req, res, next) {
  const interestId = req.params.interestId;
  try {
    const interest = await Interest.findById(interestId);
    const interestedUsers = await interest.getInterestedUsers();
    return res.json({
      interestedUsers: interestedUsers.map((user) => {
        return user.getPublicInfo;
      }),
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      error: {
        _main: "An error occurred in the server. Try again later.",
      },
    });
  }
};

exports.getInterestPage = async function (req, res, next) {
  const interestId = req.params.interestId;
  const userId = req._tokenPayload.sub;
  try {
    const interest = await Interest.findById(interestId);
    const currentUser = await User.findById(userId);
    let interestedUsers = await interest.getInterestedUsers(
      req._tokenPayload,
      user
    );

    interestedUsers = interestedUsers.map((user) => {
      return {
        id: user._id,
        name: user.name,
        age: user.age,
        profileImage: user.profileImage,
        description: user.description,
      };
    });
    return res.json({
      name: interest.name,
      id: interest._id,
      image: interest.image,
      interestedUsers,
      alreadyJoined: currentUser.interests.includes(interestId),
    });
  } catch (error) {
    if(error.message=== "")
    console.log(error);
    return res.status(500).json({
      error: {
        _main: "An error occurred in the server. Try again later.",
      },
    });
  }
};

exports.getPopular = async function (req, res, next) {
  const userId = req._tokenPayload.sub;
  try {
    const popularInterests = await Interest.find()
      .sort({ nInterestedUsers: -1 })
      .limit(50)
      .select("_id name image");
    console.log(popularInterests);
    const user = User.findById(userId);
    const userInterestsSet = new Set(user.interests);
    res.json({
      popularInterests: popularInterests.map((interest) => ({
        _id: interest._id,
        name: interest.name,
        image: interest.image,
        alreadyJoined: userInterestsSet.has(interest._id),
      })),
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      error: {
        _main: "An error occurred in the server. Try again later.",
      },
    });
  }
};
