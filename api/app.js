const dbConnection = require("./utils/db");
const express = require("express");
const cors = require("cors");
const interestsRouter = require("./routes/interests");
const userInterestsRouter = require("./routes/userInterests");
const Interest = require("./models/Interest");
const usersRouter = require("./routes/users");
const conversationsRouter = require("./routes/conversations");
const userConversationsRouter = require("./routes/userConversations");
const morgan = require("morgan");
const socketIo = require("socket.io");
const http = require("http");
const { verifyToken } = require("./utils/jwt");
const Conversation = require("./models/Conversation");

const app = express();
const httpServer = http.createServer(app);
const io = socketIo(httpServer);

app.use(morgan("tiny"));

app.use(cors());
app.use(express.json());

app.use("/interests", interestsRouter);
app.use("/userInterests", userInterestsRouter);
app.use("/users", usersRouter);
app.use("/user-conversations", userConversationsRouter);
app.use("/conversations", conversationsRouter);

io.use((socket, next) => {
  const token = socket.handshake.auth.token;
  let payload;
  try {
    payload = verifyToken(token);
  } catch (error) {
    console.log("ERROR verifying token");
    return next(new Error("Invalid token"));
  }
  socket.tokenPayload = payload;
  next();
});

io.use((socket, next) => {
  socket.join(socket.tokenPayload.sub);
  next();
});

io.on("connection", (socket) => {
  console.log("User connected");
  socket.on(
    "create conversation",
    async (receiverId, message, interestId, cb) => {
      //Have to check if receiver is interested in the interestId.
      const senderId = socket.tokenPayload.sub;
      let conversation;
      try {
        const interest = await Interest.findById(interestId);
        conversation = await Conversation.findOne({
          "users._id": { $all: [senderId, receiverId] },
        });
        let isNewConversation = false;
        if (conversation) {
          await conversation.addMessage(socket.tokenPayload, message);
        } else {
          conversation = await Conversation.create({
            users: [
              { _id: senderId, accepted: true },
              { _id: receiverId, accepted: false },
            ],
            messages: {
              text: message,
              user: senderId,
            },
            interest: { id: interestId, name: interest.name },
          });
          isNewConversation = true;
        }
        cb({
          status: "ok",
          conversationId: conversation._id,
          isNew: isNewConversation,
        });
      } catch (error) {
        console.log(error);
        cb({ status: "error" });
        return;
      }
      console.log(receiverId);
      io.to(receiverId).emit(
        "private message",
        conversation._id,
        conversation.messages[conversation.messages.length - 1]
      );
    }
  );
  socket.on("private message", async (conversationId, message, cb) => {
    const senderId = socket.tokenPayload.sub;
    let otherUsers = null;
    let conversation;
    try {
      conversation = await Conversation.findById(conversationId);
      await conversation.addMessage(socket.tokenPayload, message);
      otherUsers = conversation.users.filter(
        (user) => user._id.toString() !== senderId
      );
      cb({
        status: "ok",
        message: conversation.messages[conversation.messages.length - 1],
      });
    } catch (error) {
      console.log("Error on private message handler", error);
      cb({
        status: "error",
      });
      return;
    }
    if (otherUsers.length > 0) {
      io.to(otherUsers[0]._id.toString()).emit(
        "private message",
        conversation._id,
        conversation.messages[conversation.messages.length - 1]
      );
    }
  });
});

httpServer.listen(8000, () => {
  console.log(`Api started. Listening on port 8000`);
});

module.exports = httpServer;
