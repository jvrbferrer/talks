const mongoose = require("mongoose");
const {
  buildUrl,
  createPresignedPost,
  putObject,
} = require("../utils/s3Client");
const validators = require("../utils/validators");
const crypto = require("crypto");
const jwt = require("../utils/jwt");
const axios = require("axios");

const InterestHistoryElementSchema = new mongoose.Schema(
  {
    interest: { type: mongoose.Schema.Types.ObjectId, ref: "Interest" },
  },
  { timestamps: true }
);

const possibleGenders = ["Male", "Female", "M2F", "F2M", "Other"];
const habitPossibilities = ["Yes", "Occasionaly", "No"];
const about = {
  gender: {
    type: String,
    enum: possibleGenders,
  },
  smokes: {
    type: String,
    enum: habitPossibilities,
  },
  drinks: {
    type: String,
    enum: habitPossibilities,
  },
  fourtwenty: {
    type: String,
    enum: habitPossibilities,
  },
  location: {
    type: String,
  },
};

const preferences = {
  gender: {
    type: [{ type: String, enum: possibleGenders }],
    default: [],
  },
  withinDistance: {
    type: Number,
    validate: {
      validator: validators.validateDistance,
      message: "Not a valid distance",
    },
    default: null,
  },
  age: {
    from: { type: Number, default: null },
    to: { type: Number, default: null },
  },
};

const UserSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    email: {
      type: String,
      required: [true, "Email required"],
      unique: [true, "Email already in use"],
      lowercase: true,
      trim: true,
      validate: {
        validator: validators.validateEmail,
        message: "Not a valid email",
      },
    },
    age: {
      type: Number,
    },
    profileImage: {
      type: String,
    },
    images: {
      type: [String],
    },
    description: {
      type: String,
      trim: true,
    },
    interests: [{ type: mongoose.Schema.Types.ObjectId, ref: "Interest" }],
    usersAccepted: [{ type: mongoose.Schema.Types.ObjectId, ref: "User" }],
    usersBlocked: [{ type: mongoose.Schema.Types.ObjectId, ref: "User" }],
    interestsHistory: { type: [InterestHistoryElementSchema] },
    preferences,
    conversations: [
      { type: mongoose.Schema.Types.ObjectId, ref: "Conversation" },
    ],
    about,
    units: { type: String, enum: ["Km", "Mi"], default: "Km", required: true },
  },
  { timestamps: true }
);

UserSchema.pre("save", async function (next) {
  const correctProfileImageUrl = buildUrl(this.getProfileImageKey());
  if (this.profileImage !== correctProfileImageUrl) {
    //We changed the profile image so we will download the new url and upload it to s3.
    const res = await axios.get(this.profileImage, {
      responseType: "arraybuffer",
    });
    const profileImage = res.data;
    const awsres = await putObject(
      this.getProfileImageKey(),
      profileImage,
      res.headers["content-type"]
    );
    this.profileImage = correctProfileImageUrl;
  }
  next();
});

UserSchema.methods.authenticateMe = function authenticateMe(tokenPayload) {
  if (!tokenPayload.admin && tokenPayload.sub !== this._id.toString()) {
    throw Error("Not authorized");
  }
};

UserSchema.methods.getPublicInfo = function getPublicInfo() {
  return {
    name: this.name,
    age: this.age,
    profileImage: this.profileImage,
    description: this.description,
  };
};

UserSchema.methods.getImages = function getImages() {
  return this.images.map((key) => buildUrl(key));
};

UserSchema.methods.createImage = async function createImage(
  imageName,
  requesterTokenPayload
) {
  this.authenticateMe(requesterTokenPayload);
  const randomStr = crypto.randomBytes(24).toString();
  const imageKey = `user-images/${this._id}/${randomStr}-${imageName}`;
  const postData = await createPresignedPost(imageKey);
  this.images.push(imageKey);
  await this.save();
  return postData;
};

UserSchema.methods.deleteImage = async function deleteImage(imageKey) {
  this.authenticateMe(requesterTokenPayload);
  this.images.pull({});
};

UserSchema.methods.getHomepageInfo = async function getHomepageInfo(
  requesterTokenPayload
) {
  this.authenticateMe(requesterTokenPayload);
  await this.populate("interests", "name,image").execPopulate();
  return {
    profileImage: this.profileImage,
    name: this.name,
    interests: this.interests,
  };
};

UserSchema.methods.hasInterest = function hasInterest(
  requesterTokenPayload,
  interestId
) {
  this.authenticateMe(requesterTokenPayload);
  if (this.populated("interests")) {
    interestsIds = this.interests.map((interest) => interest._id.toString());
  } else {
    interestsIds = this.interests.map((interest) => interest.toString());
  }
  return interestsIds.includes(interestId);
};

UserSchema.methods.updateDescription = async function updateDescription(
  requesterTokenPayload,
  newDescription
) {
  this.authenticateMe(requesterTokenPayload);
  this.description = newDescription;
  return await this.save();
};

UserSchema.methods.updatePreferences = async function updatePreferences(
  requesterTokenPayload,
  newPreferences,
  units
) {
  console.log(newPreferences, units);
  this.authenticateMe(requesterTokenPayload);
  this.preferences = newPreferences;
  if (units && units !== "") {
    this.units = units;
  }
  return await this.save();
};

UserSchema.methods.getUserpageInfo = async function getUserpageInfo(
  requesterTokenPayload
) {
  this.authenticateMe(requesterTokenPayload);
  await this.populate("interests", "name image").execPopulate();
  console.log(this.interests);
  return {
    name: this.name,
    age: this.age,
    profileImage: this.profileImage,
    description: this.description,
    preferences: {
      age: this.preferences.age,
      withinDistance: this.preferences.withinDistance,
      units: this.units,
      gender: this.preferences.gender,
    },
    interests: this.interests,
  };
};

UserSchema.methods.generateToken = function generateToken() {
  return jwt.generateToken(this._id.toString());
};

UserSchema.methods.getProfileImageKey = function getProfilePictureUrl() {
  return `profile-images/${this._id}.jfif`;
};

const User = mongoose.model("User", UserSchema);

module.exports = User;
