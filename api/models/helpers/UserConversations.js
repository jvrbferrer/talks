const User = require("../User");
const Conversation = require("../Conversation");

exports.create = async function (tokenPayload, newConversation) {
  const creators = newConversation.users.filter((user) => {
    return user.accepted;
  });
  if (
    !tokenPayload.admin &&
    (creators.length !== 1 || creators[0].id !== tokenPayload.sub)
  ) {
    //There can only be 1 creator and that one must be the token subject
    throw Error("Not authorized");
  }
  const conversation = await Conversation.create(newConversation);
  await conversation.populate("users").execPopulate();
  conversation.users.forEach((user) => {
    user.conversations.push(conversation._id);
    user.save();
  });
  return conversation;
};

exports.getPreviews = async function (tokenPayload, userId) {
  if (!tokenPayload.admin && userId !== tokenPayload.sub) {
    throw Error("Not authorized");
  }
  let userConversations = await Conversation.find({
    "users._id": { $all: [userId] },
  }).populate("users._id", "profileImage name");

  userConversations = userConversations.map((conversation) => {
    let read = false;
    const users = conversation.users.map((user) => {
      if (user._id._id.toString() === tokenPayload.sub) {
        read = user.read;
      }
      return {
        _id: user._id._id,
        name: user._id.name,
        image: user._id.profileImage,
      };
    });
    return {
      id: conversation._id,
      users,
      read,
      lastMessage: conversation.messages[conversation.messages.length - 1].text,
      interest: conversation.interest.name,
    };
  });
  return userConversations;
};
