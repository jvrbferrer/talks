const User = require("../User");
const Interest = require("../Interest");

exports.create = async function (tokenPayload, userId, interestId) {
  const user = await User.findById(userId);
  const interest = await Interest.findById(interestId);
  user.authenticateMe(tokenPayload);
  if (
    user.interests.some((interest) => {
      return interest.equals(interestId);
    })
  ) {
    throw Error("User already has that interest");
  }
  user.interests.push(interestId);
  await user.save();
  interest.interestedUsers.push(userId);
  if (!interest.nInterestedUsers) {
    interest.nInterestedUsers = 0;
  }
  interest.nInterestedUsers += 1;
  await interest.save();
  return { user, interest };
};

exports.delete = async function (tokenPayload, userId, interestId) {
  const user = await User.findById(userId);
  const interest = await Interest.findById(interestId);
  user.authenticateMe(tokenPayload);
  user.interests.remove(interestId);
  interest.interestedUsers.remove(userId);
  await user.save();
  await interest.save();
  interest.nInterestedUsers -= 1;
  return { user, interest };
};
