const mongoose = require("mongoose");
const validators = require("../utils/validators");
const { createPresignedPost, buildUrl } = require("../utils/s3Client");
const crypto = require("crypto");
const NotJoinedError = require("../errors/NotJoined");

const InterestSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Interest name required"],
      unique: [true, "Interest already exists"],
      text: true,
    },
    image: {
      type: String,
    },
    interestedUsers: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
      },
    ],
    nInterestedUsers: { type: Number, default: 0 },
    suggestedImages: { type: [String] },
    creator: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
  },
  { timestamps: true }
);

InterestSchema.index({ name: "text", name: "text" });

InterestSchema.methods.authenticateParticipant = function authenticateParticipant(
  requesterTokenPayload,
  user
) {
  if (
    !requesterTokenPayload.admin &&
    !user.hasInterest(requesterTokenPayload)
  ) {
    throw Error("Not participat");
  }
};

InterestSchema.methods.addSuggestedImage = async function addSuggestedPicture(
  imagename
) {
  const randomStr = crypto.randomBytes(24).toString();
  const imageKey = `suggested-pictures/${this._id}/${randomStr}-${imagename}`;
  const postData = await createPresignedPost(imageKey);
  this.suggestedPictures.push(imageKey);
  await this.save();
  return postData;
};

InterestSchema.methods.getSuggestedImages = async function getSuggestedPictures() {
  return this.suggestedPictures.map((key) => buildUrl(key));
};

InterestSchema.methods.setImage = async function setImage(newImage) {
  this.image = newImage;
  return await this.save();
};

InterestSchema.methods.getInterestedUsers = async function getInterestedUsers(
  requesterTokenPayload,
  user
) {
  this.authenticateParticipant(requesterTokenPayload, user);
  await this.populate("interestedUsers").execPopulate();
  return this.interestedUsers;
};

const Interest = mongoose.model("Interest", InterestSchema);

module.exports = Interest;
