const mongoose = require("mongoose");

const MessageSchema = new mongoose.Schema(
  {
    text: { type: String, required: true },
    user: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  },
  { timestamps: true }
);

const ConversationSchema = new mongoose.Schema(
  {
    users: [
      {
        _id: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
        accepted: Boolean,
        read: { type: Boolean },
      },
    ],
    messages: [MessageSchema],
    interest: {
      id: { type: mongoose.Schema.Types.ObjectId, ref: "Interest" },
      name: String,
    },
  },
  { timestamps: true }
);

ConversationSchema.methods.authenticateParticipant = function authenticateParticipant(
  tokenPayload
) {
  if (tokenPayload.admin) {
    return;
  }
  if (this.populated("users._id")) {
    if (
      !this.users.find((user) => user._id._id.toString() === tokenPayload.sub)
    ) {
      throw Error("Not authenticated");
    }
  } else {
    if (!this.users.find((user) => user._id.toString() === tokenPayload.sub)) {
      throw Error("Not authenticated");
    }
  }
};

ConversationSchema.methods.addMessage = async function addMessage(
  tokenPayload,
  message
) {
  this.authenticateParticipant(tokenPayload);
  this.messages.push({ user: tokenPayload.sub, text: message });
  for (user of this.users) {
    if (user._id !== tokenPayload.sub) {
      user.read = false;
    } else {
      user.read = true;
    }
  }
  return await this.save();
};

ConversationSchema.methods.read = async function read(
  tokenPayload,
  save = true
) {
  console.log("Gonna read");
  this.authenticateParticipant(tokenPayload);
  for (user of this.users) {
    let userId;
    if (this.populated("users._id")) {
      userId = user._id._id;
    } else {
      userId = user._id;
    }
    if (userId.toString() === tokenPayload.sub) {
      console.log("Read");
      user.read = true;
    }
  }
  if (save) {
    this.save();
  }
};

ConversationSchema.statics.get = async function get(
  tokenPayload,
  conversationId
) {
  const conversation = await this.findById(conversationId).populate(
    "users._id",
    "name profileImage"
  );
  conversation.authenticateParticipant(tokenPayload);
  return conversation;
};

const Conversation = mongoose.model("Conversation", ConversationSchema);

module.exports = Conversation;
