const mongoose = require("mongoose");
const User = require("../models/User");
const jwt = require("jsonwebtoken");

let chai = require("chai");
let chaiHttp = require("chai-http");
const should = chai.should();

chai.use(chaiHttp);
let httpServer;

describe("Users", () => {
  before((done) => {
    httpServer = require("../app");
    done();
  });
  after((done) => {
    // UPDATE DON'T CLOSE THE SERVER
    User.collection.drop();
    httpServer.close(() => {
      mongoose.disconnect().then(() => {
        delete require.cache[require.resolve("../app")];
        done();
      });
    });
  });

  it("Authenticate with good token", async () => {
    res = await chai.request(httpServer).post("/users/authenticate").send({
      facebookToken:
        "EAALonsg8KTsBAEh4ZCJZBBEyaBYGH7CFA51NyK84U5G8Q4cPmRnHcvSaNh0bWTulDD6aH9FOtvmgZCN2yjEfXSXchpR4chFjngvyBtsfaZBC19w1OuucG9ReZAUGgsGUaVWHUzBdcVe1L3UN989kU6I3sPMKVaWWs0rUNqk7BWv8VB1NAx8URZChCG95kVWKJq1mz0G30YBzZASNcB9iYIWgTa4uxww2TYZCo0khXwMkXAZDZD",
    });
    res.should.have.status(200);
    const decodedToken = jwt.decode(res.body.newToken);
    const newUser = await User.findById(decodedToken.sub);
    should.equal(newUser.name, "Joaquim Ferrer");
    should.equal(newUser.email, "jvrbferrer@gmail.com");
  });

  it("Authenticate with bad token", async () => {
    res = await chai.request(httpServer).post("/users/authenticate").send({
      facebookToken:
        "EAALShfEZBZB2rdvImAFOinnUep3kgmz4IRt0yZAMAXVWCUspbryL8GZAZBzWDjoZAstc8ZC7Uv0KU4iFiGlwIPnMG0bgyZBmGXhLUC4Av4mfBdks4ZC0HfcxieJ87aCjjZAZCaIWvIfOZCMZB5wtReLGUGVd2ikQJVRGXG7qb8KaRAWbZBkitqXiPNOpGdxnjZC7M0N7GrZAHHFjZCML8gFHrgiuWcN59vYgZDZD",
    });
    res.should.have.status(500);
  });
});
