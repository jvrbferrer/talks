const usersController = require("../controllers/users");
const Router = require("express").Router;
const { verifyToken } = require("../utils/middleware");

let router = Router();

router.post("/authenticate", usersController.authenticate);

router.get("/:userId/images", verifyToken, usersController.getImages);
router.post("/:userId/images", verifyToken, usersController.createImage);
router.delete(
  "/:userId/images/:imageKey",
  verifyToken,
  usersController.deleteImage
);
router.get("/:userId/homepage", verifyToken, usersController.getHomepageInfo);
router.get("/:userId/user-page", verifyToken, usersController.getUserpageInfo);
router.post(
  "/:userId/description",
  verifyToken,
  usersController.updateDescription
);

router.post(
  "/:userId/preferences",
  verifyToken,
  usersController.updatePreferences
);

module.exports = router;
