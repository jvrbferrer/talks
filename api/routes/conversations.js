const conversationsController = require("../controllers/conversations");
const Router = require("express").Router;
const { verifyToken } = require("../utils/middleware");

let router = Router();

router.get("/:conversationId", verifyToken, conversationsController.get);

module.exports = router;
