const interestsController = require("../controllers/interests");
const Router = require("express").Router;
const { verifyToken } = require("../utils/middleware");

let router = Router();
router.get("/search/:interestName", verifyToken, interestsController.search);
router.post("/", verifyToken, interestsController.create);
router.post(
  "/:interestId/submit-image",
  verifyToken,
  interestsController.suggestImage
);
router.get(
  "/:interestId/submitted-images",
  interestsController.getSuggestedImages
);
router.post("/:interestId/set-image/", interestsController.setImage);
router.get("/:interestId/users", interestsController.getInterestedUsers);
router.get("/get-popular", verifyToken, interestsController.getPopular);
router.get(
  "/interestPage/:interestId",
  verifyToken,
  interestsController.getInterestPage
);

module.exports = router;
