const userConversationsController = require("../controllers/userConversations");
const Router = require("express").Router;
const { verifyToken } = require("../utils/middleware");

let router = Router();
router.post("/:userId", verifyToken, userConversationsController.create);
router.get(
  "/:userId/conversations-page",
  verifyToken,
  userConversationsController.getConversationsPage
);

module.exports = router;
