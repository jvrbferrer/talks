const userInterestsController = require("../controllers/userInterests");
const Router = require("express").Router;
const { verifyToken } = require("../utils/middleware");

let router = Router();
router.post(
  "/:userId/:interestId",
  verifyToken,
  userInterestsController.create
);
router.delete(
  "/:userId/:interestId",
  verifyToken,
  userInterestsController.delete
);

module.exports = router;
