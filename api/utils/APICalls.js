const axios = require("axios");
const config = require("config");

const graphPath = "https://graph.facebook.com/";

exports.inspectFBToken = (accessToken) => {
  return axios.get(
    `${graphPath}debug_token?input_token=${accessToken}&access_token=${
      config.FB_APP_ID + "|" + config.FB_APP_SECRET
    }`
  );
};

exports.getFBProfileInfo = (accessToken) => {
  return axios.get(
    `${graphPath}me?fields=id,name,email,picture.width(1000)&access_token=${accessToken}`
  );
};
