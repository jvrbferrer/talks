const mongoose = require("mongoose");
const config = require("config");

const mongoUrl = config.DBHost;

const connection = mongoose
  .connect(mongoUrl, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
  })
  .then((connection) => {
    console.log("DB Connected!");
  })
  .catch((err) => {
    console.log(`DB Connection Error: ${err.message}`);
  });

module.exports = connection;
