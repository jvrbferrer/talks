function validateEmail(mail) {
  return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail);
}

function validateDistance(distance) {
  return distance === null || distance > 0;
}

module.exports = {
  validateEmail,
  validateDistance,
};
