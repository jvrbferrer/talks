const { verifyToken: verifyJWTToken } = require("./jwt");

exports.verifyToken = (req, res, next) => {
  if (
    !req.get("authorization") ||
    req.get("authorization").split(" ")[0] != "Bearer"
  ) {
    return res.status(401).json({
      error: {
        authorization: "No token present",
      },
    });
  }
  const receivedJWTToken = req.get("authorization").split(" ")[1];
  try {
    const payload = verifyJWTToken(receivedJWTToken);
    req._tokenPayload = payload;
    next();
  } catch (error) {
    console.log(error);
    res.status(401).json({
      error: {
        authorization: "You are not authorized",
      },
    });
  }
};
