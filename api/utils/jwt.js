const jwt = require("jsonwebtoken");
const { AUTH_SECRET } = require("config");

exports.generateToken = function (userId) {
  return jwt.sign({}, AUTH_SECRET, {
    expiresIn: "7 days",
    subject: userId,
    issuer: "Talks",
    algorithm: "HS256",
  });
};

exports.verifyToken = function (token) {
  const payload = jwt.verify(token, AUTH_SECRET, {
    algorithms: ["HS256"],
    issuer: "Talks",
  });
  return payload;
};
