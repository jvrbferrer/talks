const { S3 } = require("@aws-sdk/client-s3");
const REGION = "eu-central-1";
const config = require("config");

const s3Client = new S3({ region: REGION });
const bucket = config.BUCKET;

function createPresignedPost(key) {
  let params = {
    Bucket: bucket,
    Fields: {
      key: key,
    },
    Conditions: [
      ["starts-with", "$Content-Type", "image/"],
      ["content-length-range", 1, 10485760],
    ],
    Expires: 60,
  };
  return new Promise(function (accept, reject) {
    s3Client.createPresignedPost(params, function (err, data) {
      if (err) {
        return reject(err);
      }
      return accept(data);
    });
  });
}

function buildUrl(key) {
  let baseUrl = `https://${bucket}.s3.eu-central-1.amazonaws.com/`;
  return baseUrl + key;
}

function putObject(key, object, contentType) {
  let params = {
    Bucket: bucket,
    Body: object,
    Key: key,
  };
  return new Promise(function (accept, reject) {
    s3Client.putObject(params, function (err, data) {
      if (err) {
        return reject(err);
      }
      return accept(data);
    });
  });
  return Promise.resolve(true);
}

module.exports = {
  putObject,
  createPresignedPost,
  buildUrl,
};
