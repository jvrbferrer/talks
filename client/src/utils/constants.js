const constants = {
  links: {
    conversations: "/conversations",
    conversationsConversation: "/conversations/:conversationId",
    conversationsRequests: "/conversations/requests",
    conversationsRequestsConversation:
      "/conversations/requests/:conversationId",
  },
};

export default constants;
