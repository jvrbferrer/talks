/*
If an API method calls jsonify then 
besides the response json fields being present, 
_ok means wether the request was successful.
*/
const jsonify = (response) => {
  let ok = response.ok;
  let status = response.status;
  return new Promise((accept, reject) => {
    response
      .json()
      .then((response) => {
        response._ok = ok;
        response._status = status;
        accept(response);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

const addAuthorization = (headers) => {
  if (!window.localStorage.getItem("authentication")) {
    return;
  }
  const token = JSON.parse(window.localStorage.getItem("authentication")).token;
  if (token) {
    headers.push(["Authorization", "Bearer " + token]);
  }
};

export const get = (url, params = {}) => {
  url = new URL(`/api/${url}`, window.location.origin);
  Object.keys(params).forEach((key) =>
    url.searchParams.append(key, params[key])
  );
  let headers = [["Accept", "application/json"]];
  addAuthorization(headers);
  return fetch(url, {
    headers,
    method: "GET",
    mode: "cors",
  });
};

export const post = (url, data) => {
  url = new URL(`/api/${url}`, window.location.origin);
  let headers = [
    ["Content-Type", "application/json"],
    ["Accept", "application/json"],
  ];
  addAuthorization(headers);
  return fetch(url, {
    headers,
    method: "POST",
    mode: "cors",
    body: JSON.stringify(data),
  });
};

export const delet = (url, data) => {
  url = new URL(`/api/${url}`, window.location.origin);
  let headers = [
    ["Content-Type", "application/json"],
    ["Accept", "application/json"],
  ];
  addAuthorization(headers);
  return fetch(url, {
    headers,
    method: "DELETE",
    mode: "cors",
    body: JSON.stringify(data),
  });
};

/*
{
  facebookToken
}
*/
export const authenticate = (data) => {
  return post("users/authenticate", data).then((response) => jsonify(response));
};

/*
{}
returns {
  userHomepageInfo
}
*/
export const getHomepageInfo = (userId) => {
  return get(`users/${userId}/homepage`).then((response) => jsonify(response));
};

/*
{}
returns {
  popularInterests: {_id, name, image}
}
*/
export const getPopularInterests = () => {
  return get("interests/get-popular").then((response) => jsonify(response));
};

/*
{
  interestName
}
returns {
  interestsFound: []
}
*/
export const searchInterest = (interestName) => {
  return get(`interests/search/${interestName}`).then((response) =>
    jsonify(response)
  );
};

/*
{
  name
}
returns {
  _id
}
*/
export const createInterest = (name) => {
  return post("interests/", { name: name }).then((response) => {
    return jsonify(response);
  });
};

export const getInterestPage = (id) => {
  return get(`interests/interestPage/${id}`).then((response) =>
    jsonify(response)
  );
};

export const addUserInterest = (userId, interestId) => {
  return post(`userInterests/${userId}/${interestId}`).then((response) => {
    return jsonify(response);
  });
};

export const deleteUserInterest = (userId, interestId) => {
  return delet(`userInterests/${userId}/${interestId}`).then((response) =>
    jsonify(response)
  );
};

export const getConversationsPage = (userId) => {
  return get(`user-conversations/${userId}/conversations-page`).then(
    (response) => {
      return jsonify(response);
    }
  );
};

export const getConversation = (conversationId) => {
  return get(`conversations/${conversationId}`).then((response) =>
    jsonify(response)
  );
};

export const getUserPage = (userId) => {
  return get(`/users/${userId}/user-page`).then((response) =>
    jsonify(response)
  );
};

export const updateDescription = (userId, newDescription) => {
  return post(`/users/${userId}/description`, {
    newDescription,
  }).then((response) => jsonify(response));
};

export const updatePreferences = (userId, newPreferences) => {
  return post(`/users/${userId}/preferences`, {
    newPreferences,
  }).then((response) => jsonify(response));
};
