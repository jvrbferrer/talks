import {
  useRouteMatch as useRouteMatchUtil,
  useLocation,
  matchPath,
} from "react-router-dom";
import { useMemo } from "react";

export const useRouteMatch = (path) => {
  const location = useLocation();
  const routeMatch = useRouteMatchUtil();
  const res = useMemo(() => matchPath(location.pathname, path), [
    location.pathname,
    path,
  ]);
  return path ? res : routeMatch;
};
