import LoadingWhite from "../../features/loading/Loading.module.css";
import styles from "./Loading.module.css";

const Loading = ({ active = true }) => {
  return <img src={LoadingWhite} alt="loading icon" className={styles.image} />;
};

export default Loading;
