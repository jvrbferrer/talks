import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { getHomepageInfo as getHomepageInfoAPI } from "../../utils/APICalls";

export const getHomepageInfo = createAsyncThunk(
  "user/getHomepageInfo",
  async (params, thunkAPI) => {
    try {
      const res = await getHomepageInfoAPI(
        thunkAPI.getState().authentication.authenticated.userId
      );
      if (res._ok) {
        return Promise.resolve(res);
      }
      return thunkAPI.rejectWithValue(res);
    } catch (error) {
      return thunkAPI.rejectWithValue({
        error: {
          _main:
            "We can't communicate with the server right now. Try again later",
        },
      });
    }
  }
);

const userSlice = createSlice({
  name: "user",
  initialState: {
    loading: false,
    profileImage: null,
    name: null,
    interests: [],
  },
  extraReducers: {
    [getHomepageInfo.pending]: (state, action) => {
      state.loading = true;
    },
    [getHomepageInfo.fulfilled]: (state, action) => {
      state.loading = false;
      state.profileImage = action.payload.profileImage;
      state.name = action.payload.name;
      state.interests = action.payload.interests;
    },
    [getHomepageInfo.rejected]: (state, action) => {
      state.loading = false;
    },
  },
});

export default userSlice.reducer;
