import { Formik } from "formik";
import * as Yup from "yup";
import styles from "./DefaultPreferencesForm.module.css";
import { updatePreferences } from "../../utils/APICalls";
import GenderPreferenceTextBox from "./GenderPreferenceCheckbox";
import { useSelector } from "react-redux";

const preferencesSchema = Yup.object().shape({
  hasDistance: Yup.bool(),
  withinDistance: Yup.number().nullable(),
  units: Yup.mixed().oneOf(["Km", "Mi"]),
  ageFrom: Yup.number().positive().integer().nullable(),
  ageTo: Yup.number().positive().integer().nullable(),
  hasGender: Yup.bool(),
});

const DefaultPreferencesForm = ({ defaultValues }) => {
  const userId = useSelector(
    (state) => state.authentication.authenticated.userId
  );

  const handleUpdatePreferences = (values, formikActions) => {
    updatePreferences(userId, {
      gender: values.hasGender ? values.gender : [],
      withinDistance: values.hasDistance ? values.withinDistance : null,
      units: values.hasDistance ? values.units : null,
      age: {
        from: values.hasAgeFrom ? values.ageFrom : null,
        to: values.hasAgeTo ? values.ageTo : null,
      },
    })
      .then((res) => {
        formikActions.setSubmitting(false);
        if (!res._ok) {
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <Formik
      initialValues={{
        withinDistance: defaultValues.withinDistance || "",
        ageFrom: defaultValues.age.from || "",
        ageTo: defaultValues.age.to || "",
        gender: defaultValues.gender,
        hasDistance: !!defaultValues.withinDistance,
        hasAgeFrom: !!defaultValues.age.from,
        hasAgeTo: !!defaultValues.age.to,
        hasGender: !defaultValues.gender.empty,
        units: defaultValues.units,
      }}
      onSubmit={handleUpdatePreferences}
      validationSchema={preferencesSchema}
    >
      {({
        handleSubmit,
        handleChange,
        values,
        setFieldValue,
        isSubmitting,
      }) => {
        console.log(values);
        const handleChangeGenderPreference = (event) => {
          if (event.target.checked) {
            values.gender.push(event.target.name);
          } else {
            values.gender = values.gender.filter(
              (val) => val !== event.target.name
            );
          }
          setFieldValue("gender", values.gender);
        };
        return (
          <form className={styles.searchForm} onSubmit={handleSubmit}>
            <h3>Distance</h3>
            <div className={styles.filter}>
              <input
                type="checkbox"
                onChange={handleChange}
                name="hasDistance"
                checked={values.hasDistance}
              />
              <input
                type="number"
                className={styles.distanceInput}
                value={values.withinDistance}
                onChange={handleChange}
                name="withinDistance"
                disabled={!values.hasDistance}
              />
              <select
                type="choice"
                name="units"
                onChange={handleChange}
                value={values.units}
                disabled={!values.hasDistance}
              >
                <option value="Mi">Mi</option>
                <option value="Km">Km</option>
              </select>
            </div>
            <h3>Age</h3>
            <div className={styles.filter}>
              <p>From</p>
              <input
                type="checkbox"
                onChange={handleChange}
                name="hasAgeFrom"
                checked={values.hasAgeFrom}
              />
              <input
                type="number"
                className={styles.ageInput}
                value={values.ageFrom}
                onChange={handleChange}
                name="ageFrom"
                disabled={!values.hasAgeFrom}
              />

              <p>to</p>
              <input
                type="checkbox"
                onChange={handleChange}
                name="hasAgeTo"
                checked={values.hasAgeTo}
              />
              <input
                type="number"
                className={styles.ageInput}
                value={values.ageTo}
                onChange={handleChange}
                name="ageTo"
                disabled={!values.hasAgeTo}
              />
            </div>
            <h3>Gender</h3>
            <input
              type="checkbox"
              onChange={handleChange}
              name="hasGender"
              checked={values.hasGender}
            />
            <div className={styles.GenderFilter}>
              <GenderPreferenceTextBox
                disabled={!values.hasGender}
                name="Female"
                label="Female"
                onChange={handleChangeGenderPreference}
                value={values.gender.includes("Female")}
              />
              <GenderPreferenceTextBox
                disabled={!values.hasGender}
                name="Male"
                label="Male"
                onChange={handleChangeGenderPreference}
                value={values.gender.includes("Male")}
              />
              <GenderPreferenceTextBox
                disabled={!values.hasGender}
                name="M2F"
                label="M2F"
                onChange={handleChangeGenderPreference}
                value={values.gender.includes("M2F")}
              />
              <GenderPreferenceTextBox
                disabled={!values.hasGender}
                name="F2M"
                label="F2M"
                onChange={handleChangeGenderPreference}
                value={values.gender.includes("F2M")}
              />
              <GenderPreferenceTextBox
                disabled={!values.hasGender}
                name="Other"
                label="Other"
                onChange={handleChangeGenderPreference}
                value={values.gender.includes("Other")}
              />
            </div>
            <button
              className={styles.submitButton}
              type="submit"
              disabled={isSubmitting}
            >
              Save
            </button>
          </form>
        );
      }}
    </Formik>
  );
};

export default DefaultPreferencesForm;
