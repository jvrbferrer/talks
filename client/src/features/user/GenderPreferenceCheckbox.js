import styles from "./GenderPreferenceCheckbox.module.css";

const GenderPreferenceTextBox = ({
  name,
  label,
  onChange,
  value,
  disabled,
}) => {
  return (
    <div className={styles.genderPossibilityContainer}>
      <p>{label}</p>
      <input
        type="checkbox"
        onChange={onChange}
        name={name}
        checked={value}
        disabled={disabled}
      />
    </div>
  );
};

export default GenderPreferenceTextBox;
