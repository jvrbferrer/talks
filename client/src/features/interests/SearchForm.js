import { Formik } from "formik";
import * as Yup from "yup";
// import { useHistory } from "react-router-dom";
import styles from "./SearchForm.module.css";
import { useDispatch } from "react-redux";
import { searchInterest } from "./interestsSlice";

const SearchSchema = Yup.object().shape({
  searchName: Yup.string().max(20, "Name too long").required(),
});

const SearchForm = () => {
  const dispatch = useDispatch();

  const onSearch = (values, formikActions) => {
    if (values.searchName === "") {
      return;
    }
    dispatch(searchInterest(values.searchName)).then(() => {
      formikActions.setSubmitting(false);
    });
  };

  return (
    <Formik
      initialValues={{ searchName: "" }}
      onSubmit={onSearch}
      validationSchema={SearchSchema}
    >
      {({ handleSubmit, handleChange, values }) => {
        return (
          <form className={styles.searchForm} onSubmit={handleSubmit}>
            <input
              onChange={handleChange}
              value={values.searchName}
              name="searchName"
              className={styles.searchInput}
              placeholder="Search interest"
            />
            <button type="submit" className={styles.searchButton}>
              Go
            </button>
          </form>
        );
      }}
    </Formik>
  );
};

export default SearchForm;
