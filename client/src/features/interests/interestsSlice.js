import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { searchInterest as searchInterestAPI } from "../../utils/APICalls";

export const searchInterest = createAsyncThunk(
  "interests/searchInterest",
  async (searchName, thunkAPI) => {
    try {
      const res = await searchInterestAPI(searchName);
      console.log("res from api", res);
      if (!res._ok) {
        return thunkAPI.rejectWithValue(res.error);
      }
      return res.interests;
    } catch (error) {
      console.log(error);
      return thunkAPI.rejectWithValue(error);
    }
  }
);

const interestsSlice = createSlice({
  initialState: {
    search: {
      name: "",
      loading: false,
      searched: false,
      interests: [],
    },
  },
  name: "interests",
  extraReducers: {
    [searchInterest.pending]: (state, action) => {
      state.search.name = action.meta.arg;
      state.search.searching = true;
      state.search.searched = true;
      return state;
    },
    [searchInterest.fulfilled]: (state, action) => {
      state.search.searching = false;
      state.search.interests = action.payload;
    },
    [searchInterest.rejected]: (state, action) => {
      state.search.searching = false;
    },
  },
});

export default interestsSlice.reducer;
