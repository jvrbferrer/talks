import styles from "./InterestCard.module.css";
import plusSign from "../../assets/images/UtilIcons/Add.svg";

const NewInterestCard = ({ name, ...props }) => {
  return (
    <div className={styles.mainContainer} {...props}>
      <img alt="plusSymbol" src={plusSign} className={styles.plusSign} />
      <p className={styles.name}>{name} (create new)</p>
    </div>
  );
};

export default NewInterestCard;
