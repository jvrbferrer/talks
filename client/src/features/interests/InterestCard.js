import styles from "./InterestCard.module.css";
import { getRandomTopicImage } from "./utils";

const InterestCard = ({ name, imageUrl = null, alreadyJoined }) => {
  imageUrl = imageUrl === null ? getRandomTopicImage() : imageUrl;
  return (
    <div className={styles.mainContainer}>
      <img alt="name" src={imageUrl} className={styles.image} />
      <p className={styles.name}>{name}</p>
      {alreadyJoined && <p>Joined</p>}
    </div>
  );
};

export default InterestCard;
