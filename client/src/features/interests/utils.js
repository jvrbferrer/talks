import literatureLogo from "../../assets/images/Topics/Literature.svg";
import sportsLogo from "../../assets/images/Topics/Sports.svg";
import musicLogo from "../../assets/images/Topics/Music.svg";

export const getRandomTopicImage = () => {
  const possibleTopics = [literatureLogo, sportsLogo, musicLogo];
  return possibleTopics[Math.floor(Math.random()) * possibleTopics.length];
};
