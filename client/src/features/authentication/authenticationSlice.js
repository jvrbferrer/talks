import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { authenticate as authenticateAPI } from "../../utils/APICalls";
import jwtDecode from "jwt-decode";

const getInitialState = () => {
  return {
    loading: false,
    authenticated: {
      value: false,
      token: "",
      expirationDate: null,
      userId: "",
    },
  };
};

export const logoutFacebook = createAsyncThunk(
  "authentication/logoutFacebook",
  async (params, thunkAPI) => {
    try {
      window.FB.logout();
      return Promise.resolve();
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const authenticate = createAsyncThunk(
  "authentication/authenticate",
  async (params, thunkAPI) => {
    try {
      const res = await authenticateAPI({
        facebookToken: params.facebookToken,
      });
      if (res._ok) {
        return Promise.resolve(res.newToken);
      }
      return thunkAPI.rejectWithValue({ error: res.error });
    } catch (error) {
      console.log(error);
      return thunkAPI.rejectWithValue({
        error: {
          _main:
            "We can't communicate with the server right now. Try again later.",
        },
      });
    }
  }
);

const authenticationSlice = createSlice({
  name: "authentication",
  initialState: getInitialState(),
  reducers: {
    loadState: (state, action) => {
      const savedAuthentication = window.localStorage.getItem("authentication");
      if (savedAuthentication) {
        state.authenticated = JSON.parse(savedAuthentication);
        return state;
      }
      return state;
    },
    logout: (state, action) => {
      window.localStorage.removeItem("authentication");
      state = getInitialState();
      return state;
    },
  },
  extraReducers: {
    [authenticate.pending]: (state, action) => {
      state.loading = true;
    },
    [authenticate.fulfilled]: (state, action) => {
      state.loading = false;
      const token = action.payload;
      let decodedToken = jwtDecode(token);
      let expirationDate = decodedToken.exp * 1000;
      state.authenticated.value = true;
      state.authenticated.token = token;
      state.authenticated.expirationDate = expirationDate;
      state.authenticated.userId = decodedToken.sub;
      window.localStorage.setItem(
        "authentication",
        JSON.stringify(state.authenticated)
      );
    },
    [authenticate.rejected]: (state, action) => {
      state.loading = false;
    },
    [logoutFacebook.pending]: (state, action) => {
      state.loading = true;
    },
    [logoutFacebook.fulfilled]: (state, action) => {
      window.localStorage.removeItem("authentication");
      state = getInitialState();
      return state;
    },
    [logoutFacebook.rejected]: (state, action) => {
      window.localStorage.removeItem("authentication");
      state = getInitialState();
      return state;
    },
  },
});

export default authenticationSlice.reducer;
export const { loadState, logout } = authenticationSlice.actions;
