import styles from "./UserCard.module.css";

const UserCard = ({
  image,
  name,
  age,
  description,
  userId,
  onClick,
  ...props
}) => {
  return (
    <div className={styles.mainContainer} onClick={onClick} {...props}>
      <p className={styles.name}>{name}</p>
      <p className={styles.age}>{age}</p>
      <p className={styles.description}> {description} </p>
      <img alt="" src={image} className={styles.image} />
    </div>
  );
};

export default UserCard;
