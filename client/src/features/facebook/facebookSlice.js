import { createSlice } from "@reduxjs/toolkit";

const facebookSlice = createSlice({
  name: "facebook",
  initialState: {
    loaded: false,
  },
  reducers: {
    setLoaded: (state, action) => {
      state.loaded = true;
      return state;
    },
  },
});

export default facebookSlice.reducer;
export const { setLoaded } = facebookSlice.actions;
