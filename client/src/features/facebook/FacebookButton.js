import styles from "./FacebookButton.module.css";
import fbLogo from "../../assets/images/OtherLogos/f_logo_RGB-White_1024.svg";
import { useSelector } from "react-redux";
// import { authenticate } from "../authentication/authenticationSlice";

const FacebookButton = ({ children, className = "" }) => {
  const facebookLoaded = useSelector((state) => state.facebook.loaded);
  // const dispatch = useDispatch();
  const handleClick = () => {
    window.FB.login(
      (response) => {
        // if (response.status === "connected") {
        //   dispatch(
        //     authenticate({ facebookToken: response.authResponse.accessToken })
        //   );
        // }
      },
      { scope: "email,public_profile" }
    );
  };

  let containerClassName = styles.mainContainer + " " + className;
  if (!facebookLoaded) {
    containerClassName += " " + styles.inactive;
  }

  return (
    <div className={containerClassName} onClick={handleClick}>
      <img className={styles.logo} src={fbLogo} alt="FacebookLogo" />
      <div className={styles.textContainer}>
        <p className={styles.text}>{children}</p>
      </div>
    </div>
  );
};

export default FacebookButton;
