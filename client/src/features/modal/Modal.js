import { useRef, useEffect } from "react";
import styles from "./Modal.module.css";
import { useDispatch } from "react-redux";
import { close } from "./modalSlice";
import closeButton from "../../assets/images/UtilIcons/Close.svg";

const Modal = ({ children, className }) => {
  const modalRef = useRef(null);
  const dispatch = useDispatch();

  useEffect(() => {
    const handleClick = (event) => {
      if (modalRef.current && !modalRef.current.contains(event.target)) {
        dispatch(close());
      }
    };

    document.addEventListener("mousedown", handleClick);
    return () => {
      document.removeEventListener("mousedown", handleClick);
    };
  }, [modalRef, dispatch]);

  return (
    <div className={styles.modalContainer}>
      <div className={styles.innerContainer} ref={modalRef}>
        <div className={styles.nav}>
          <img
            src={closeButton}
            alt="close modal button"
            onClick={() => dispatch(close())}
            className={styles.closeButton}
          />
        </div>
        {children}
      </div>
    </div>
  );
};

export default Modal;
