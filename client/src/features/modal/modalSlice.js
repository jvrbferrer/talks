import { createSlice } from "@reduxjs/toolkit";
import CreateConversationModal from "../../pages/CreateConversationModal/CreateConversationModal";

const modalSlice = createSlice({
  name: "modal",
  initialState: {
    name: null,
    info: null,
  },
  reducers: {
    close: (state) => {
      state.name = null;
    },
    openCreateConversation: (state, action) => {
      state.name = "createConversation";
      state.info = {
        image: action.payload.image,
        name: action.payload.name,
        age: action.payload.age,
        description: action.payload.description,
        userId: action.payload.userId,
        interestId: action.payload.interestId,
        interestName: action.payload.interestName,
      };
    },
  },
});

export const nameToModal = (modalState) => {
  switch (modalState.name) {
    case "createConversation":
      return <CreateConversationModal {...modalState.info} />;
    case null:
      return null;
    default:
      console.log("Modal not identified");
      return null;
  }
};

export const { close, openCreateConversation } = modalSlice.actions;
export default modalSlice.reducer;
