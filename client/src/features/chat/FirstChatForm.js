import { useState } from "react";
import styles from "./FirstChatForm.module.css";
import { useHistory } from "react-router-dom";
import constants from "../../utils/constants";
import { chatHandleTextareaKeyPress } from "./utils";

const FirstChatForm = ({ userId, interestId, onSuccess }) => {
  const [text, setText] = useState("");
  const history = useHistory();
  const onTextareaKeyPress = chatHandleTextareaKeyPress;
  const onTextareaChange = (event) => {
    setText(event.target.value);
  };
  const onSubmit = (event) => {
    event.preventDefault();
    window.socket.emit(
      "create conversation",
      userId,
      text,
      interestId,
      ({ status, conversationId, isNew }) => {
        if (status === "ok") {
          history.push(`${constants.links.conversations}/${conversationId}`);
        }
      }
    );
  };

  return (
    <form className={styles.mainContainer} onSubmit={onSubmit}>
      <textarea
        className={styles.chatInput}
        value={text}
        onChange={onTextareaChange}
        onKeyPress={onTextareaKeyPress}
      />
      <button className={styles.button} type="submit">
        Send
      </button>
    </form>
  );
};

export default FirstChatForm;
