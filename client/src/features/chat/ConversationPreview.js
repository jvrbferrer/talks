import styles from "./ConversationPreview.module.css";

const ConversationPreview = ({ name, lastMessage, image, interest, read }) => {
  return (
    <div className={styles.mainContainer}>
      <img className={styles.image} src={image} alt="profile" />
      <p className={styles.name}>{name}</p>
      <p className={`${styles.message} ${read ? "" : styles.notRead}`}>
        {lastMessage}
      </p>
      <p className={styles.interest}>{interest}</p>
    </div>
  );
};

export default ConversationPreview;
