import { useState } from "react";
import { chatHandleTextareaKeyPress } from "./utils";
import styles from "./ChatForm.module.css";

const ChatForm = ({ conversationId, onSuccess }) => {
  const [text, setText] = useState("");
  const [loading, setLoading] = useState(false);

  const onTextareaChange = (event) => {
    setText(event.target.value);
  };

  const onSubmit = (event) => {
    event.preventDefault();
    setLoading(true);
    if (loading || text === "") {
      return;
    }
    try {
      window.socket.emit(
        "private message",
        conversationId,
        text,
        ({ status, message }) => {
          setLoading(false);
          if (status === "ok") {
            onSuccess(message);
            setText("");
          } else {
          }
        }
      );
    } catch (error) {
      setLoading(false);
      console.log(error);
    }
  };
  return (
    <form className={styles.mainContainer} onSubmit={onSubmit}>
      <textarea
        className={styles.chatInput}
        value={text}
        onChange={onTextareaChange}
        onKeyPress={chatHandleTextareaKeyPress}
        placeholder="Type a message here"
      />
      <button className={styles.button} type="submit">
        Send
      </button>
    </form>
  );
};

export default ChatForm;
