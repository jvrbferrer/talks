import { createSlice } from "@reduxjs/toolkit";

const chatSlice = createSlice({
  name: "chat",
  initialState: {
    /*{
      conversationId: "text",
    }*/
    newMessages: {},
  },
  reducers: {
    receiveMessage: (state, action) => {
      const conversationId = action.payload.conversationId;
      state.newMessages[conversationId] = action.payload.message;
      return state;
    },
    setRead: (state, action) => {
      state.newMessages = action.payload;
      return state;
    },
    readMessage: (state, action) => {
      delete state.newMessages[action.payload];
      return state;
    },
  },
});

export default chatSlice.reducer;
export const { receiveMessage, readMessage, setRead } = chatSlice.actions;
