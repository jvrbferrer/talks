import { useEffect, useLayoutEffect } from "react";
import { Switch, Route, useLocation } from "react-router-dom";
import styles from "./App.module.css";
import { useDispatch, useSelector } from "react-redux";
import {
  loadState,
  authenticate,
  logout,
} from "./features/authentication/authenticationSlice";
import { receiveMessage } from "./features/chat/chatSlice";
import { setLoaded as setFacebookLoaded } from "./features/facebook/facebookSlice";
import AuthenticatedPage from "./pages/AuthenticatedPage/AuthenticatedPage";
import Homepage from "./pages/Homepage/Homepage";
import InterestPage from "./pages/InterestPage/InterestPage";
import ConversationsPage from "./pages/ConversationsPage/ConversationsPage";
import { getHomepageInfo } from "./features/user/userSlice";
import { nameToModal, close } from "./features/modal/modalSlice";
import io from "socket.io-client";
import constants from "./utils/constants";
import UserPage from "./pages/UserPage/UserPage";

const App = () => {
  const dispatch = useDispatch();
  // const facebookLoaded = useSelector((state) => state.facebook.loaded);
  const authenticated = useSelector(
    (state) => state.authentication.authenticated
  );
  const activeModal = nameToModal(useSelector((state) => state.modal));
  const location = useLocation();

  //Load redux state
  useEffect(() => {
    dispatch(loadState());
  }, [dispatch]);

  //Close modal everytime the url changes
  useLayoutEffect(() => {
    dispatch(close());
  }, [location, dispatch]);

  //GetHomepageInfo everytime we authenticate
  useEffect(() => {
    if (authenticated.value) {
      dispatch(getHomepageInfo());
    }
  }, [authenticated.value, dispatch]);

  useEffect(() => {
    if (window.fbInited) {
      dispatch(setFacebookLoaded());
    } else {
      window.addEventListener("facebook-loaded", () => {
        dispatch(setFacebookLoaded());
      });
    }
    window.addEventListener("facebook-auth-answer", (e) => {
      const fbRes = e.detail;
      console.log("response from fb auth answer ", fbRes);
      if (fbRes.status === "connected") {
        dispatch(
          authenticate({ facebookToken: fbRes.authResponse.accessToken })
        );
      }
    });
  }, [dispatch]);

  useEffect(() => {
    if (authenticated.value) {
      window.socket = io({
        auth: {
          token: authenticated.token,
        },
      });
      window.socket.on("connect_error", (error) => {
        console.log("Can't connect socket io.", error);
        if (error.message === "Invalid token") {
          dispatch(logout());
        }
        setTimeout(() => {
          window.socket.connect();
        }, 300000);
      });
      window.socket.on("disconnect", (reason) => {
        console.log("disconnect", reason);
      });
      window.socket.on("private message", (conversationId, message) => {
        dispatch(receiveMessage({ conversationId, message }));
      });
      return () => window.socket.disconnect();
    } else if (window.socket && window.socket.connected) {
      window.socket.disconnect();
    }
  }, [authenticated.value, authenticated.token, dispatch]);

  // useLayoutEffect(() => {
  //   window.FB.Event.subscribe("auth.statusChange", (res) => {
  //     dispatch(authenticate(res.authResponse.accessToken));
  //   });

  //   // window.FB.getLoginStatus((res) => {
  //   //   if (res.status === "connected") {
  //   //   } else {
  //   //     dispatch(logout());
  //   //   }
  //   // });
  // }, [dispatch, facebookLoaded]);

  return (
    <div className={styles.mainContainer} id="mainContainer">
      {activeModal}
      <Switch>
        <Route exact path={["/", "/search/:searchedInterest"]}>
          <AuthenticatedPage>
            <Homepage />
          </AuthenticatedPage>
        </Route>
        <Route exact path={"/interests/:interestId"}>
          <AuthenticatedPage>
            <InterestPage />
          </AuthenticatedPage>
        </Route>
        <Route exact path={"/profile"}>
          <AuthenticatedPage>
            <UserPage />
          </AuthenticatedPage>
        </Route>
        <Route
          exact
          path={[
            constants.links.conversations,
            constants.links.conversationsRequests,
            constants.links.conversationsConversation,
            constants.links.conversationsRequestsConversation,
          ]}
        >
          <AuthenticatedPage>
            <ConversationsPage />
          </AuthenticatedPage>
        </Route>
      </Switch>
    </div>
  );
};

export default App;
