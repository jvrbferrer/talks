import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";
import authenticationReducer from "./features/authentication/authenticationSlice";
import facebookReducer from "./features/facebook/facebookSlice";
import userReducer from "./features/user/userSlice";
import interestsReducer from "./features/interests/interestsSlice";
import modalReducer from "./features/modal/modalSlice";
import chatReducer from "./features/chat/chatSlice";

const store = configureStore({
  reducer: combineReducers({
    authentication: authenticationReducer,
    facebook: facebookReducer,
    user: userReducer,
    interests: interestsReducer,
    modal: modalReducer,
    chat: chatReducer,
  }),
});

window.store = store;

export default store;
