import styles from "./Homepage.module.css";
import { useLayoutEffect, useState } from "react";
import { createInterest, getPopularInterests } from "../../utils/APICalls";
import NewInterestCard from "../../features/interests/NewInterestCard";
import InterestCard from "../../features/interests/InterestCard";
import { useSelector } from "react-redux";
import { useHistory, Link } from "react-router-dom";

const Homepage = () => {
  const [popularInterests, setPopularInterests] = useState([]);
  const searchedInterestsState = useSelector((state) => state.interests.search);
  const history = useHistory();

  useLayoutEffect(() => {
    getPopularInterests()
      .then((res) => {
        setPopularInterests(res.popularInterests);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  let searchedInterestsSection = null;
  if (searchedInterestsState.searched) {
    searchedInterestsSection = (
      <div className={styles.searchedInterestsContainer}>
        {searchedInterestsState.interests.map((interest) => {
          return (
            <Link
              to={`/interests/${interest._id}`}
              key={interest._id}
              className={styles.interestLink}
            >
              <InterestCard
                name={interest.name}
                imageUrl={interest.image}
                alreadyJoined={interest.alreadyJoined}
              />
            </Link>
          );
        })}
        <NewInterestCard
          name={searchedInterestsState.name}
          onClick={() => {
            createInterest(searchedInterestsState.name)
              .then((res) => {
                history.push(`interests/${res._id}`);
              })
              .catch((error) => {
                console.log(error._main);
              });
          }}
        />
        ;
      </div>
    );
  }

  console.log(popularInterests);
  return (
    <div className={styles.mainContainer}>
      {searchedInterestsState.searched && searchedInterestsSection}
      <p className={styles.title}>Popular interests</p>
      <div className={styles.interestsContainer}>
        {popularInterests.map((interest) => {
          return (
            <Link
              to={`/interests/${interest._id}`}
              key={interest._id}
              className={styles.interestLink}
            >
              <InterestCard
                name={interest.name}
                imageUrl={interest.image}
                key={interest._id}
                alreadyJoined={interest.alreadyJoined}
              />
            </Link>
          );
        })}
      </div>
    </div>
  );
};

export default Homepage;
