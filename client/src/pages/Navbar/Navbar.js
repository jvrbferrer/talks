import logoWithNameWhite from "../../assets/images/TalksImage/LogoWithNameWhite.svg";
import { useSelector, useDispatch } from "react-redux";
import styles from "./Navbar.module.css";
import { logoutFacebook } from "../../features/authentication/authenticationSlice";
import { NavLink, Link } from "react-router-dom";
import SearchForm from "../../features/interests/SearchForm";

const Navbar = () => {
  const dispatch = useDispatch();
  const onLogout = () => {
    dispatch(logoutFacebook());
  };

  const newMessages = useSelector((state) => state.chat.newMessages);
  let newMessagesExist = false;
  /* eslint-disable no-unused-vars */
  for (let i in newMessages) {
    newMessagesExist = true;
    break;
  }
  /* eslint-enable no-unused-vars */

  const user = useSelector((state) => state.user);
  if (user.loading || !user.name) {
    return null;
  }

  return (
    <div className={styles.mainContainer}>
      <Link to="/">
        <img className={styles.logo} alt="Talks" src={logoWithNameWhite} />
      </Link>
      <SearchForm />
      <NavLink
        to="/profile"
        className={`${styles.navlink} ${styles.profileLink}`}
        activeClassName={styles.activeNavlink}
      >
        <img
          src={user.profileImage}
          alt="profile"
          className={styles.profileImage}
        />
        {user.name.split(" ")[0]}
      </NavLink>
      <NavLink
        to="/"
        exact
        className={styles.navlink}
        activeClassName={styles.activeNavlink}
      >
        Interests
      </NavLink>
      <NavLink
        to="/conversations"
        className={`${styles.navlink} ${styles.conversationsLink}`}
        activeClassName={styles.activeNavlink}
      >
        {newMessagesExist && (
          <figure className={styles.notificationSymbol}></figure>
        )}
        Conversations
      </NavLink>
      <button onClick={onLogout}>Log out</button>
    </div>
  );
};

export default Navbar;
