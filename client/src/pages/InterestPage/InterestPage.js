import { getInterestPage } from "../../utils/APICalls";
import { getRandomTopicImage } from "../../features/interests/utils";
import { useState, useEffect } from "react";
import styles from "./InterestPage.module.css";
import Loading from "../../features/loading/Loading";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import { addUserInterest } from "../../utils/APICalls";
import UserCard from "../../features/otherUsers/UserCard";
import { useDispatch } from "react-redux";
import { openCreateConversation } from "../../features/modal/modalSlice";

const InterestPage = ({ alreadyJoined }) => {
  const [loading, setLoading] = useState(true);
  const [joinLoading, setJoinLoading] = useState(false);
  const [interestInfo, setInterestInfo] = useState(null);
  const params = useParams();
  const dispatch = useDispatch();
  const userId = useSelector(
    (state) => state.authentication.authenticated.userId
  );

  const interestId = params.interestId;
  const interestMyself = (e) => {
    e.preventDefault();
    setJoinLoading(true);
    addUserInterest(userId, interestInfo.id)
      .then((res) => {
        setJoinLoading(false);
        if (res._ok) {
          setInterestInfo({ ...interestInfo, alreadyJoined: true });
        } else {
          console.log(res.error);
        }
      })
      .catch((error) => {
        setJoinLoading(false);
        console.log(error);
      });
  };

  useEffect(() => {
    getInterestPage(interestId)
      .then((res) => {
        res.image = res.image ? res.image : getRandomTopicImage();
        setInterestInfo(res);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
        console.log(error);
      });
  }, [interestId, alreadyJoined]);

  if (loading || !interestInfo) {
    return (
      <div className={styles.mainContainer}>
        <Loading />;
      </div>
    );
  }

  return (
    <div className={styles.mainContainer}>
      <img alt="" src={interestInfo.image} className={styles.image} />
      <p className={styles.name}>{interestInfo.name}</p>
      <button
        className={styles.joinButton}
        disabled={interestInfo.alreadyJoined || joinLoading}
        onClick={interestMyself}
      >
        {interestInfo.alreadyJoined ? "Joined" : "Join"}
      </button>
      <div className={styles.peopleContainer}>
        {!interestInfo.alreadyJoined && (
          <p>Join to talk to people about this common Interest!</p>
        )}
        {interestInfo.alreadyJoined &&
          interestInfo.interestedUsers.map((user) => {
            const userProps = {
              image: user.profileImage,
              name: user.name,
              age: user.age,
              description: user.description,
              userId: user.id,
            };
            return (
              <UserCard
                key={user.id}
                {...userProps}
                onClick={() =>
                  dispatch(
                    openCreateConversation({
                      ...userProps,
                      interestId: interestId,
                      interestName: interestInfo.name,
                    })
                  )
                }
              />
            );
          })}
      </div>
    </div>
  );
};

export default InterestPage;
