import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { getUserPage } from "../../utils/APICalls";
import styles from "./UserPage.module.css";
import { updateDescription, deleteUserInterest } from "../../utils/APICalls";
import InterestCard from "../../features/interests/InterestCard";
import { Link } from "react-router-dom";
import DefaultPreferencesForm from "../../features/user/DefaultPreferencesForm";

const UserPage = () => {
  console.log("Render user page");
  const [userInfo, setuserInfo] = useState({
    user: null,
    loading: true,
    error: null,
  });
  const [descriptionTextArea, setDescriptionTextArea] = useState({
    value: "",
    editing: false,
    submitting: false,
  });
  const userId = useSelector(
    (state) => state.authentication.authenticated.userId
  );
  useEffect(() => {
    getUserPage(userId).then((res) => {
      if (res._ok) {
        setuserInfo({ user: res, loading: false, error: null });
        setDescriptionTextArea({
          value: res.description,
          editing: false,
          submitting: false,
        });
      } else {
        console.log(res);
        setuserInfo({ user: null, loading: false, error: res.error });
      }
    });
  }, [userId]);
  const onTextareaChange = (event) => {
    setDescriptionTextArea({
      ...descriptionTextArea,
      value: event.target.value,
    });
  };

  const onUpdateDescriptionSubmit = (event) => {
    event.preventDefault();
    setDescriptionTextArea({ ...descriptionTextArea, submitting: true });
    updateDescription(userId, descriptionTextArea.value)
      .then((res) => {
        if (res._ok) {
          setDescriptionTextArea({
            ...descriptionTextArea,
            submitting: false,
            editing: false,
          });
        } else {
          console.log(res);
          setDescriptionTextArea({ ...descriptionTextArea, submitting: false });
        }
      })
      .catch((error) => {
        console.log(error);
        setDescriptionTextArea({ ...descriptionTextArea, submitting: false });
      });
  };

  const onDeleteInterestSubmit = (event, userId, interestId) => {
    event.preventDefault();
    deleteUserInterest(userId, interestId)
      .then((res) => {
        if (res._ok) {
          const interests = userInfo.user.interests.filter(
            (interest) => interest._id !== interestId
          );
          setuserInfo({ ...userInfo, user: { ...userInfo.user, interests } });
        } else {
          console.log(res);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  if (userInfo.loading || !userInfo.user) {
    return null;
  }
  return (
    <div className={styles.mainContainer}>
      <div className={styles.userInfo}>
        <img
          className={styles.profileImage}
          src={userInfo.user.profileImage}
          alt="Facebook profile"
        />
        <p className={styles.name}>{userInfo.user.name}</p>
        <p className={styles.age}>{userInfo.user.age}</p>
        <div
          className={styles.descriptionContainer}
          onClick={() => {
            if (!descriptionTextArea.editing) {
              setDescriptionTextArea({
                ...descriptionTextArea,
                editing: true,
              });
            }
          }}
        >
          <p>About you</p>
          {descriptionTextArea.editing ? (
            <form
              className={styles.descriptionForm}
              onSubmit={onUpdateDescriptionSubmit}
            >
              <textarea
                className={styles.descriptionTextarea}
                value={descriptionTextArea.value}
                onChange={onTextareaChange}
              >
                {descriptionTextArea.value}
              </textarea>
              <button className={styles.button} type="submit">
                Update
              </button>
            </form>
          ) : (
            <p className={styles.descriptionP}>{descriptionTextArea.value}</p>
          )}
        </div>
      </div>
      <div className={styles.interests}>
        <h2>Added interests</h2>
        <div className={styles.interestsContainer}>
          {userInfo.user.interests.map((interest) => {
            return (
              <div className={styles.interestDiv}>
                <Link
                  to={`/interests/${interest._id}`}
                  key={interest._id}
                  className={styles.interestLink}
                >
                  <InterestCard
                    name={interest.name}
                    imageUrl={interest.image}
                    key={interest._id}
                  />
                </Link>
                <form
                  className={styles.deleteInterestForm}
                  onSubmit={(event) =>
                    onDeleteInterestSubmit(event, userId, interest._id)
                  }
                >
                  <button type="submit" className={styles.deleteInterestButton}>
                    Delete
                  </button>
                </form>
              </div>
            );
          })}
        </div>
      </div>
      <div className={styles.defaultFiltersContainer}>
        <DefaultPreferencesForm defaultValues={userInfo.user.preferences} />
      </div>
    </div>
  );
};

export default UserPage;
