import styles from "./CreateConversationModal.module.css";
import Modal from "../../features/modal/Modal";
import FirstChatForm from "../../features/chat/FirstChatForm";

const CreateConversationModal = ({
  image,
  name,
  age,
  description,
  userId,
  interestName,
  interestId,
}) => {
  return (
    <Modal>
      <div className={styles.mainContainer}>
        <img src={image} alt="user" className={styles.image} />
        <p className={styles.name}>{name}</p>
        <p className={styles.description}>{description}</p>
        <p className={styles.interestName}>{interestName}</p>
        <FirstChatForm userId={userId} interestId={interestId} />
      </div>
    </Modal>
  );
};

export default CreateConversationModal;
