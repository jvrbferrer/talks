import { useSelector } from "react-redux";
import Navbar from "../Navbar/Navbar";
import LandingPage from "../LandingPage/LandingPage";

const AuthenticatedPage = ({ children }) => {
  const authenticated = useSelector((state) => {
    return state.authentication.authenticated;
  });

  if (!authenticated.value) {
    return <LandingPage />;
  }

  return (
    <>
      <Navbar />
      {children}
    </>
  );
};

export default AuthenticatedPage;
