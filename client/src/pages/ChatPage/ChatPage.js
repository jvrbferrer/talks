import { useEffect, useState, useLayoutEffect } from "react";
import { getConversation } from "../../utils/APICalls";
import { useSelector, useDispatch } from "react-redux";
import styles from "./ChatPage.module.css";
import ChatForm from "../../features/chat/ChatForm";
import { readMessage } from "../../features/chat/chatSlice";

const ChatPage = ({ conversationId, setLastMessage }) => {
  const [conversation, setConversation] = useState({
    conversation: null,
    shouldScroll: true,
    loading: true,
  });
  const dispatch = useDispatch();
  const userId = useSelector(
    (state) => state.authentication.authenticated.userId
  );

  const newMessage = useSelector(
    (state) => state.chat.newMessages[conversationId]
  );

  const checkShouldScroll = () => {
    let messagesContainerRef = document.getElementById("messagesContainer");
    if (
      Math.abs(
        messagesContainerRef.scrollTop +
          messagesContainerRef.clientHeight -
          messagesContainerRef.scrollHeight
      ) < 30
    ) {
      return true;
    }
    return false;
  };

  useEffect(() => {
    if (conversation.conversation && newMessage) {
      conversation.conversation.messages.push(newMessage);
      setConversation({ ...conversation, shouldScroll: checkShouldScroll() });
      setLastMessage(newMessage);
      dispatch(readMessage(conversationId));
    }
  }, [newMessage, conversation, conversationId, dispatch, setLastMessage]);

  useEffect(() => {
    getConversation(conversationId).then((conversation) => {
      console.log("Received conversation");
      setConversation({
        conversation: conversation,
        shouldScroll: true,
        loading: false,
      });
      dispatch(readMessage(conversationId));
    });
  }, [conversationId]);

  useLayoutEffect(() => {
    let messagesContainerRef = document.getElementById("messagesContainer");
    if (conversation.shouldScroll && messagesContainerRef) {
      messagesContainerRef.scrollTop = messagesContainerRef.scrollHeight;
      setConversation({ ...conversation, shouldScroll: false });
    }
  }, [conversation, setConversation]);

  if (conversation.loading) {
    return null;
  }
  return (
    <div className={styles.mainContainer}>
      <div className={styles.messagesContainer} id="messagesContainer">
        {conversation.conversation.messages.map((message, index) => {
          let classToUse;
          if (message.user === userId) {
            classToUse = styles.messageSent;
          } else {
            classToUse = styles.messageReceived;
          }
          if (index === conversation.conversation.messages.length - 1) {
            classToUse += " " + styles.lastMessage;
          }
          return (
            <div key={message._id} className={classToUse}>
              {message.text}
            </div>
          );
        })}
      </div>
      <ChatForm
        conversationId={conversationId}
        onSuccess={(newMessage) => {
          conversation.conversation.messages.push(newMessage);
          setConversation({
            ...conversation,
            shouldScroll: checkShouldScroll(),
          });
          setLastMessage(newMessage);
        }}
      />
    </div>
  );
};

export default ChatPage;
