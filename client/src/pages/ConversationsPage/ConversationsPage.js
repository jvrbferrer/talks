import { useEffect, useState, useLayoutEffect, useRef } from "react";
import styles from "./ConversationsPage.module.css";
import { NavLink, Link } from "react-router-dom";
import { useRouteMatch } from "../../utils/customHooks";
import constants from "../../utils/constants";
import { getConversationsPage } from "../../utils/APICalls";
import { useDispatch, useSelector } from "react-redux";
import ConversationPreview from "../../features/chat/ConversationPreview";
import ChatPage from "../ChatPage/ChatPage";
import { setRead } from "../../features/chat/chatSlice";

const ConversationsPage = () => {
  const [conversationsPreviews, setConversationsPreviews] = useState(null);
  const [conversationId, setConversationId] = useState(null);
  const prevMainContainerStyle = useRef(null);
  const dispatch = useDispatch();

  let readObject = useSelector((state) => state.chat.newMessages);

  useLayoutEffect(() => {
    const mainContainer = document.getElementById("mainContainer");
    prevMainContainerStyle.current = mainContainer.style;
    mainContainer.style.height = "100vh";
    mainContainer.style.overflowY = "hidden";
    return () => (mainContainer.style = prevMainContainerStyle.current);
  }, [prevMainContainerStyle]);

  const requestsMatch = useRouteMatch({
    path: constants.links.conversationsRequests,
    exact: true,
  });
  const requestsConversationMatch = useRouteMatch({
    path: constants.links.conversationsRequestsConversation,
    exact: true,
  });
  const conversationsConversationMatch = useRouteMatch({
    path: constants.links.conversationsConversation,
    exact: true,
  });
  // const conversationsMatch = useRouteMatch({
  //   path: constants.links.conversations,
  //   exact: true,
  // });
  //Tricky stuff but only easy way I see to do it cleanly without unecessary rerenders
  //Start from biggest match
  let newConversationId = null;
  let currentLinkBeg = "";
  if (requestsConversationMatch) {
    newConversationId = requestsConversationMatch.params.conversationId;
    currentLinkBeg = constants.links.conversationsRequests;
  } else if (requestsMatch) {
    currentLinkBeg = constants.links.conversationsRequests;
  } else if (conversationsConversationMatch) {
    newConversationId = conversationsConversationMatch.params.conversationId;
    currentLinkBeg = constants.links.conversations;
  } else {
    currentLinkBeg = constants.links.conversations;
  }
  if (conversationId !== newConversationId) {
    setConversationId(newConversationId);
  }

  const userId = useSelector(
    (state) => state.authentication.authenticated.userId
  );
  useEffect(() => {
    getConversationsPage(userId).then((results) => {
      setConversationsPreviews(results.conversations);
      let nextReadObject = {};
      for (let conversation of results.conversations) {
        if (!conversation.read) {
          nextReadObject[conversation.id] = conversation.lastMessage;
        }
      }
      dispatch(setRead(nextReadObject));
    });
  }, [userId, dispatch]);

  if (!conversationsPreviews) {
    return null;
  }
  const messages = conversationsPreviews.map((conversation) => {
    const otherUser = conversation.users.filter(
      (user) => user.id !== userId
    )[0];
    return (
      <Link to={`${currentLinkBeg}/${conversation.id}`} key={conversation.id}>
        <ConversationPreview
          name={otherUser.name}
          lastMessage={conversation.lastMessage}
          image={otherUser.image}
          interest={conversation.interest}
          read={!readObject[conversation.id]}
        />
      </Link>
    );
  });

  return (
    <div className={styles.mainContainer}>
      <div className={styles.contactsContainer}>
        <div className={styles.contactsNav}>
          <NavLink
            strict
            to={constants.links.conversations}
            className={styles.contactNavLink}
            activeClassName={styles.activeContactNavLink}
            isActive={() => !requestsConversationMatch && !requestsMatch}
          >
            Messages
          </NavLink>
          <NavLink
            strict
            to={constants.links.conversationsRequests}
            className={styles.contactNavLink}
            activeClassName={styles.activeContactNavLink}
            isActive={() => requestsConversationMatch || requestsMatch}
          >
            Requests
          </NavLink>
        </div>
        {messages}
      </div>
      {conversationId && (
        <ChatPage
          conversationId={conversationId}
          setLastMessage={(newMessage) => {
            let activeConversation = conversationsPreviews.find(
              (conversation) => {
                return conversation.id === conversationId;
              }
            );
            activeConversation.lastMessage = newMessage.text;
            setConversationsPreviews([...conversationsPreviews]);
          }}
        />
      )}
    </div>
  );
};

export default ConversationsPage;
