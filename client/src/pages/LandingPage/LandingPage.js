import styles from "./LandingPage.module.css";
import coverImage from "../../assets/images/Cover/CoverLanding.png";
import logoWithName from "../../assets/images/TalksImage/LogoWithName.svg";
import FacebookButton from "../../features/facebook/FacebookButton";

const LandingPage = () => {
  return (
    <div className={styles.mainContainer}>
      <div className={styles.coverImageDiv}>
        <img
          src={coverImage}
          alt="People talking"
          className={styles.coverImage}
          draggable="false"
        />
      </div>
      <img
        src={logoWithName}
        alt="Talks"
        draggable="false"
        className={styles.logoWithName}
      />
      <div className={styles.facebookButtonContainer}>
        <FacebookButton>Continue with facebook</FacebookButton>
      </div>
    </div>
  );
};

export default LandingPage;
